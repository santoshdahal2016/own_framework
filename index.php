<?php

require_once __DIR__.'/app/Module/Protiofilo/Controllers/ProtiofiloController.php';
$loader = require 'vendor/autoload.php';
$loader->register();

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APP\Event\RequestEvent;
use App\Framework\Core;


$request = Request::createFromGlobals();

// Our framework is now handling itself the request
$app = new Core();

$app->on('request', function (RequestEvent $event) {
    // let's assume a proper check here
    if ('admin' == $event->getRequest()->getPathInfo()) {
        echo 'Access Denied!';
        exit;
    }
});



$app->map('/about', 'App\\Module\\Protiofilo\\Controllers\\ProtiofiloController::about');

$app->map('/', 'App\\Module\\Protiofilo\\Controllers\\ProtiofiloController::home');

$app->map('/skills', 'App\\Module\\Protiofilo\\Controllers\\ProtiofiloController::skills');

$app->map('/contact', 'App\\Module\\Protiofilo\\Controllers\\ProtiofiloController::contact');


$response = $app->handle($request);
$response->send();