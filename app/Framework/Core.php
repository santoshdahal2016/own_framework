<?php

namespace App\Framework;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Event\RequestEvent;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;


class Core implements HttpKernelInterface
{
    /** @var RouteCollection */
    protected $routes;
    protected $dispatcher;

    public function __construct()
    {
        $this->routes = new RouteCollection();
        $this->dispatcher = new EventDispatcher();
    }

    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        // create a context using the current request
        $context = new RequestContext();
        $context->fromRequest($request);

        $event = new RequestEvent();
        $event->setRequest($request);

        $this->dispatcher->dispatch('request', $event);

        $matcher = new UrlMatcher($this->routes, $context);
        $resolver = new ControllerResolver();
        $argumentResolver = new ArgumentResolver();


        try {

            $attributes = $matcher->match($request->getPathInfo());
            $request->attributes->add($attributes);
            $controller = $resolver->getController($request);
            $arguments = $argumentResolver->getArguments($request, $controller);
            $response = call_user_func_array($controller, $arguments);

        } catch (ResourceNotFoundException $e) {
            $response = new Response('Not found!', Response::HTTP_NOT_FOUND);
        }

        return $response;
    }

    public function on($event, $callback)
    {
        $this->dispatcher->addListener($event, $callback);
    }

    public function fire($event)
    {
        return $this->dispatcher->dispatch($event);
    }

    public function map($path, $controller)
    {
        $this->routes->add($path, new Route(
            $path,
            array('_controller' => $controller)
        ));
    }

}