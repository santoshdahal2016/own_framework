<?php

namespace App\Module\Protiofilo\Controllers;

use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\Templating\PhpEngine;
//use Symfony\Component\Templating\TemplateNameParser;
//use Symfony\Component\Templating\Loader\FilesystemLoader;
use Twig_Environment;
use Twig_Loader_Filesystem;


class  ProtiofiloController
{
//    public $filesystemLoader, $templating ,
        public  $twig;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(
            array (
                __DIR__ . '/../../../../Views',
                __DIR__ . '/../Views'
            )
        );
        // set up environment
        $params = array(
            'auto_reload' => true, // disable cache
            'autoescape' => true
        );
        $this->twig = new Twig_Environment($loader, $params);



//        $this->filesystemLoader = new FilesystemLoader(__DIR__ . '/../../../../view/%name%');
//        $this->templating = new PhpEngine(new TemplateNameParser(), $this->filesystemLoader);

    }


    public function home()
    {

//        ob_start();
//        include sprintf(__DIR__ . '/../../../../Views/Partials/head.html');
//        include sprintf(__DIR__ . '/../../../../Views/index1.html');
//        return new Response(ob_get_clean());

        $html = $this->twig->render('index.html');


//        $html = $this->templating->render('index.html');

        return new Response($html);

    }

    public function about()
    {

        $html = $this->twig->render('aboutme.html');
        return new Response($html);

    }

    public function skills()
    {

        $html = $this->twig->render('skills.html');
        return new Response($html);

    }

    public function contact()
    {

        $html = $this->twig->render('contact.html');
        return new Response($html);

    }
}
